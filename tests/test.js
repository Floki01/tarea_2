const assert = require('assert');
const mult = require('../src/index');

describe('multiplicacion', function(){
    it('Deberia retornar 5 para 5 * 1', function(){
        assert.strictEqual(mult(5,1), 5)
    });

    it('Deberia retornar 100 para 50 * 2', function(){
        assert.strictEqual(mult(50,2), 100)
    });
})